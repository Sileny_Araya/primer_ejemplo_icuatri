/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2019;

import java.util.Scanner;

/**
 *
 * @author estudiante
 */
public class Proyecto2019 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       new Proyecto2019().numeroSuerte();
       new Proyecto2019().numeroPerfecto();
        
    }
    
    public void numeroSuerte (){
        Scanner sc = new Scanner(System.in);
        int dia, mes, año, suerte, suma, cifra1, cifra2, cifra3, cifra4;
        System.out.println("Introduzca fecha de nacimiento");
        System.out.print("día: ");
        dia = sc.nextInt();
        System.out.print("mes: ");
        mes = sc.nextInt();
        System.out.print("año: ");
        año = sc.nextInt();
        suma = dia + mes + año;
        cifra1 = suma/1000;      
        cifra2 = suma/100%10;  
        cifra3 = suma/10%10; 
        cifra4 = suma%10;   
        suerte = cifra1 + cifra2 + cifra3 + cifra4;
        System.out.println("Su número de la suerte es: " + suerte);
    }
    
    public void numeroPerfecto(){
        Scanner sc = new Scanner(System.in);
        int num;
        int suma = 0;
        System.out.println("Introduzca un numero");
        System.out.print("numero: ");
        num= sc.nextInt();
        
        for(int i = 1; i > num - 1; i++){
            if (num % i == 0)
                suma += i;
            
        }
        if( suma == num){
            System.out.println("El numero "+ num+ " es un numero perfecto");
        }else{
            System.out.println("El numero "+ num+ " es un numero imperfecto");
        }
    
 }  

}
