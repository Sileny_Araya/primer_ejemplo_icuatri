/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion.de.productos;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class Productos {
    
    public String fechaCaducidad, numeroLote;

    public Productos(String fechaCaducidad, String numeroLote) {
        this.fechaCaducidad = fechaCaducidad;
        this.numeroLote = numeroLote;
    }
    
    public String getAtributos() {
        return "Fecha de caducidad: " + fechaCaducidad
                + "\nNumero de Loto: " + numeroLote;
    }
    
        public void tipoProducto() {

        Scanner ingresar = new Scanner(System.in);
        while (true) {
            System.out.println("\n1.Ingresar productos frescos\n2.Ingresar productos refrigerados"
                    + "\n3.Ingresar productos congelados\n4.Atras\n"+ "\nSeleccione una opción: ");
            int opcion = ingresar.nextInt();
            switch (opcion) {
                case 1:
                    ProdFrescos prodfres = new ProdFrescos(" ", " ", " ", " ");
                    prodfres.datosProdFres();
                    prodfres.tipoProducto();
                    tipoProducto();

                case 2:
                    ProductosRefrigerados prodRefri = new ProductosRefrigerados(" ", " ", 0, 0, " ", " ");
                    prodRefri.datosProdRefri();
                    prodRefri.tipoProducto();
                    tipoProducto();
                case 3:
                    ProdCongelados prodCong = new ProdCongelados(" ", " ", " ", " ", 0);
                    prodCong.datosProdCongelados();
                    prodCong.tipoProducto();

                case 4:
                    System.exit(0);

                default:
                    System.out.println("Error! Digite opción válida!!");
                    break;
            }
        }

    }
}
