/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestion.de.productos;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class ProdFrescos extends Productos{
    
    public String fechaEnvasado, paisOrigen;
            
    public ProdFrescos(String fechaCaducidad, String numeroLote, String fechaEnvasado, String paisOrigen) {
        super(fechaCaducidad, numeroLote);
        this.fechaEnvasado = fechaEnvasado;
        this.paisOrigen = paisOrigen;
    }
    
    public String getAtributos() {
        return "Fecha de Caducidad: " + fechaCaducidad
                + "\nNumero de Loto: " + numeroLote
                + "\nFecha de Envasado:" + fechaEnvasado
                + "\nPais de Origen: " + paisOrigen;
    }
    
    public void datosProdFres (){
        Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduzca la fecha de caducidad del producto: ");
            fechaCaducidad = sc.next();
            System.out.println("Introduzca el numero de lote del producto: ");
            numeroLote = sc.next();
            System.out.println("Introduzca la fecha de envasado del producto: ");
            fechaEnvasado = sc.next();
            System.out.println("Introduzca el pais de origen del producto: ");
            paisOrigen = sc.next();
            
            System.out.println("\nProducto ingresado correctamente ");
            
    }
    
     //polimorfismo
    public void tipoProducto() {
        System.out.println("El Producto es fresco");
    }
}
