/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

import java.util.Scanner;

/**
 *
 * @author Sileny Araya
 */
public class Entrenador extends SeleccionFutbol{
    
    public String idFereracion;

    public Entrenador( int id, String nombre, String apellidos, int edad,String idFereracion) {
        super(id, nombre, apellidos, edad);
        this.idFereracion = idFereracion;
    }
    
    public String getAtributos() {
        return "Id: " + id
                + "\nNombre: " + nombre
                + "\nApellidos:" + apellidos
                + "\nEdad: " + edad
                + "\nIdFederacion: " + idFereracion;
    }
    
    //polimorfismo
    public void tipoPersona() {
        System.out.println("la persona es entrenador");
    }
    
      public void dirigirPartido (){
         System.out.println("Al entrenador "+ nombre+ " le toca dirigir el partido la proxima semana");
     }
     public void dirigirEntrenamiento (){
         System.out.println("Al entrenador "+ nombre+ " le toca dirigir el entrenamiento mañana");
         SeleccionFutbol s =new SeleccionFutbol(0, " ", " ", 0);
         s.tipoPersona();
     }

    
    public void datosEntre (){
        Scanner sc = new Scanner(System.in);
            System.out.println("\nIntroduzca el id del entrenador: ");
            id = sc.nextInt();
            System.out.println("Introduzca el nombre del entrenador: ");
            nombre = sc.next();
            System.out.println("Introduzca el apellido del entrenador: ");
            apellidos = sc.next();
            System.out.println("Introduzca la edad del entrenador: ");
            edad = sc.nextInt();
            System.out.println("Introduzca el id de federacion del entrenador: ");
            idFereracion = sc.next();
            
            System.out.println("\nEntrenador ingresado correctamente ");
            
    }
    
}
